class Lighter {
	constructor(containerSelector, itemSelector) {
		this.container_selector = containerSelector;
		this.item_selector = itemSelector;
		this.container = document.querySelector(this.container_selector);
		this.items = this.container.querySelectorAll(this.item_selector);
		this.length = this.items.length;
		this.currentIndex = null;
		this.open = false;
		this.currentImg = null;
		this.lightImg = null;
		this.init();
		this.hammerElem = null;
		this.updateHammer = null;
		this.imgHigh = false;
		this.windowHigh = true;
		this.viewportHeight = null;
		this.viewportWidth = null;
		this.imgRatio = null;
		this.viewportRatio = null;
	}
	
	init() {
		let lighterObj = this;

		$(document).on('click', this.container_selector + ' ' + this.item_selector, function(event) {
			lighterObj.currentIndex = $(this).closest('.slick-slide').index() -1;
			lighterObj.currentImg = this;

			lighterObj.openLightBox(lighterObj.currentImg);
		});
		
		$(document).on('click', '.lighter-container .lighter--bg, .lighter-container .lighter--close', () => {
			this.closeLightBox();
		});

		var resizeTimeout;
		window.addEventListener('resize', function() {
			if (!lighterObj.open) {
				return;
			}

			clearTimeout(resizeTimeout);
			resizeTimeout = setTimeout(function() {
				lighterObj.resize();
			}, 250);
		});
	}
	resize() {
		let lighterObj = this;

		lighterObj.closeLightBox(true);
	}
	closeLightBox(reOpen = false) {
		if (!this.open) {
			return;
		}
		
		this.hammerElem.off('swipeleft swiperight pinch pinchend pinchstart swipedown');

		let lighterObj = this;
		
		$('.lighter-container .lighter--img').animate({
			marginTop: '100px',
			opacity: 0
		}, 250);

		$('.lighter-container').animate({
			opacity: 0,
		}, 250, function() {
			this.remove();
			lighterObj.open = false;
			if (reOpen) {
				lighterObj.openLightBox(lighterObj.items[lighterObj.currentIndex])
			}
		});
	}
	initPinch() {
		var lighterObj = this;

		this.hammerElem.get('pinch').set({ enable: true });
		this.hammerElem.get('swipe').set({ direction: Hammer.DIRECTION_ALL });

		this.lightImg.style.height = 'auto';
		this.lightImg.style.width = 'auto';

		var fixHammerjsDeltaIssue = undefined;
		var pinchStart = { x: undefined, y: undefined }
		var lastEvent = undefined;

		var viewportHeight = this.viewportHeight,
			viewportWidth = this.viewportWidth;

		var viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
			viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

		this.windowHigh = this.viewportHeight > this.viewportWidth;

		var $lightImg = $('.lighter--img');

		this.realSize = {
			width: this.lightImg.width,
			height: this.lightImg.height
		}

		this.imgHigh = this.realSize.width < this.realSize.height;

		var originalSize = {
			width: viewportWidth,
			height: this.lightImg.height * (viewportWidth / this.lightImg.width)
		}

		var current = {
			x: viewportWidth * 0.5,
			y: viewportHeight * 0.5,
			z: 1,
			zooming: false,
			width: null,
			height: null,
		}

		this.fullWidth = function() {
			lighterObj.lightImg.style.height = 'auto';
			lighterObj.lightImg.style.width = '100%';

			current.x = lighterObj.lightImg.width * -0.5;

			originalSize = {
				width: viewportWidth,
				height: lighterObj.lightImg.height * (viewportWidth / lighterObj.lightImg.width)
			};
		}

		this.fullHeight = function() {
			lighterObj.lightImg.style.height = '100%';
			lighterObj.lightImg.style.width = 'auto';

			originalSize = {
				width: lighterObj.lightImg.width * (viewportHeight / lighterObj.lightImg.height),
				height: viewportHeight
			};
		}

		this.origiSize = function() {
			lighterObj.lightImg.style.height = 'auto';
			lighterObj.lightImg.style.width = 'auto';

			current.width = lighterObj.realSize.width;
			current.height = lighterObj.realSize.height;

			originalSize = {
				width: lighterObj.realSize.width,
				height: lighterObj.realSize.height
			};
		}

		this.imgRatio = this.realSize.width / this.realSize.height;
		this.viewportRatio = viewportWidth / viewportHeight;


		if (this.realSize.width < viewportWidth && this.realSize.height < viewportHeight) {
			current.x = this.realSize.width * -0.5
			this.origiSize();
		} else {
			if (this.imgRatio > this.viewportRatio) {
				this.fullWidth();
			} else {
				current.x = ((viewportHeight / this.realSize.height) * lighterObj.lightImg.width) * -0.5
				this.fullHeight();
			}
		}

	
		var last = {
			x: current.x,
			y: current.y,
			z: current.z
		}

		function getRelativePosition(element, point, originalSize, scale) {
			var domCoords = getCoords(element);

			var elementX = point.x - domCoords.x;
			var elementY = point.y - domCoords.y;

			var relativeX = elementX / (originalSize.width * scale / 2) - 1;
			var relativeY = elementY / (originalSize.height * scale / 2) - 1;
			return { x: relativeX, y: relativeY }
		}

		function getCoords(elem) { // crossbrowser version
			var box = elem.getBoundingClientRect();

			var body = document.body;
			var docEl = document.documentElement;

			var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
			var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

			var clientTop = docEl.clientTop || body.clientTop || 0;
			var clientLeft = docEl.clientLeft || body.clientLeft || 0;

			var top  = box.top +  scrollTop - clientTop;
			var left = box.left + scrollLeft - clientLeft;

			return { x: Math.round(left), y: Math.round(top) };
		}

		function scaleFrom(zoomOrigin, currentScale, newScale) {
			var currentShift = getCoordinateShiftDueToScale(originalSize, currentScale);
			var newShift = getCoordinateShiftDueToScale(originalSize, newScale)

			var zoomDistance = newScale - currentScale

			var shift = {
				x: currentShift.x - newShift.x,
				y: currentShift.y - newShift.y,
			}

			var output = {
				x: zoomOrigin.x * shift.x,
				y: zoomOrigin.y * shift.y,
				z: zoomDistance
			}
			return output
		}

		function getCoordinateShiftDueToScale(size, scale){
			var newWidth = scale * size.width;
			var newHeight = scale * size.height;
			var dx = (newWidth - size.width) / 2
			var dy = (newHeight - size.height) / 2
			return {
				x: dx,
				y: dy
			}
		}

		this.hammerElem.on('pinch', function(e) {
			var d = scaleFrom(pinchZoomOrigin, last.z, last.z * e.scale)
			current.x = d.x + last.x + e.deltaX;
			current.y = d.y + last.y + e.deltaY;
			current.z = d.z + last.z;
			lastEvent = 'pinch';
			lighterObj.updateHammer();
		})

		var pinchZoomOrigin = undefined;
		this.hammerElem.on('pinchstart', function(e) {
			pinchStart.x = e.center.x;
			pinchStart.y = e.center.y;
			pinchZoomOrigin = getRelativePosition(lighterObj.lightImg, { x: pinchStart.x, y: pinchStart.y }, originalSize, current.z);
			lastEvent = 'pinchstart';
		})

		this.hammerElem.on('pinchend', function(e) {
			last.x = current.x;
			last.y = current.y;
			last.z = current.z;
			lastEvent = 'pinchend';

			current.x = viewportWidth * -0.5;
			current.y = viewportHeight * 0.5;
			current.z = 1;
			current.zooming = false;
			current.width = lighterObj.realSize.width;
			current.height = lighterObj.realSize.height;

			if (lighterObj.realSize.width < viewportWidth && lighterObj.realSize.height < viewportHeight) {
				current.x = lighterObj.realSize.width * -0.5
				lighterObj.origiSize();
			} else {
				if (lighterObj.imgRatio > lighterObj.viewportRatio) {
					lighterObj.fullWidth();
				} else {
					current.x = ((viewportHeight / lighterObj.realSize.height) * lighterObj.realSize.width) * -0.5
					lighterObj.fullHeight();
				}
			}

			last.x = current.x;
			last.y = current.y;
			last.z = current.z;

			lighterObj.lightImg.style.transition = 'all .25s'
			lighterObj.lightImg.style.WebkitTransition = 'all .25s';
			lighterObj.lightImg.style.MozTransition = 'all .25s';
			
			lighterObj.updateHammer();
			
			setTimeout(function() {
				lighterObj.lightImg.style.transition = 'initial'
				lighterObj.lightImg.style.WebkitTransition = 'initial';
				lighterObj.lightImg.style.MozTransition = 'initial';
			}, 250);
		})

		lighterObj.updateHammer = function() {
			current.height = originalSize.height * current.z;
			current.width = originalSize.width * current.z;

			if (current.z < 1) {
				current.z = 1;
			}
			
			lighterObj.lightImg.style.transform = "translate3d(calc("+ current.x +"px), calc("+ current.y +"px - 50%), 0) scale(" + current.z + ")";
		}
		
		lighterObj.updateHammer();
	}
	openLightBox(img) {
		var lighterObj = this;
		// NEW LIGHTBOX
		if (!this.open) {
			$('body').css({overflow: 'hidden'});
			
			let html = '<div class="lighter-container"><div class="lighter--bg"></div><div class="lighter--close"></div><div class="lighter--counter">-/-</div><img class="lighter--img" src="'+img.getAttribute('data-src')+'"></div>';
			
			$(html).appendTo("body").hide().fadeIn(350);
		} else {
			//REMOVE EVENT LISTENERS
			this.hammerElem.off('swipeleft swiperight pinch pinchend pinchstart swipedown');
			this.lightImg.src = img;
			this.lightImg.style.opacity = 0;
		}

		this.lightImg = document.querySelector('.lighter--img');

		this.lightImg.onload = loaded;

		function loaded() {
			lighterObj.lightImg.classList.remove('lighter--changing');

			lighterObj.initPinch();

			lighterObj.lightImg.style.transition = 'opacity .25s';
			lighterObj.lightImg.style.webkitTransition = 'opacity .25s';
			lighterObj.lightImg.style.opacity = 1;

			setTimeout(function() {
				lighterObj.lightImg.style.transition = '';
				lighterObj.lightImg.style.webkitTransition = '';
			}, 260);
		}

		let i = lighterObj.currentIndex + 1;
		$('.lighter-container .lighter--counter').html(i+'/'+lighterObj.length);
		

		this.hammerElem = new Hammer(lighterObj.lightImg);

		this.hammerElem.on("swipedown", function(ev) {
			lighterObj.closeLightBox();
		});


		this.hammerElem.on("swipeleft swiperight", function(ev) {		
			if (ev.type == "swiperight") {
				if (lighterObj.currentIndex < 1) {
					lighterObj.currentIndex = lighterObj.items.length -1;
				} else {
					lighterObj.currentIndex --;
				}
			} else if (ev.type == "swipeleft") {
				if (lighterObj.currentIndex < lighterObj.items.length -1) {
					lighterObj.currentIndex ++;
				} else {
					lighterObj.currentIndex = 0;
				}
			}

			lighterObj.lightImg.classList.add('lighter--changing');
			lighterObj.openLightBox(lighterObj.items[lighterObj.currentIndex].getAttribute('data-src'))
		});

		this.open = true;
	}
}